pragma solidity >=0.4.21 <0.6.0;

 // El contrato Ownable tiene una dirección owner, provee de control de autorización basica
 // para las funciones,
contract Ownable {
    address private _owner;

    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );

    // En el constructor se ajusta `owner` a la dirección que crea el contrato
    constructor() internal {
        _owner = msg.sender;
        emit OwnershipTransferred(address(0), _owner);
    }


    function owner() public view returns(address) {
        return _owner;
    }

    // Modificador que añadiremos a las funciones para proteger
    // las funciones que queramos que solamente el owner las llame
    modifier onlyOwner() {
        require(isOwner(), "msg.sender != owner");
        _;
    }

    // Nos permite comprobar si la persona que manda la transacción
    // es el owner del contrato
    function isOwner() public view returns(bool) {
        return msg.sender == _owner;
    }

    // Esta función nos permitirá que si somos el owner renunciar al control
    // del contrato, la dirección del owner se configurará a 0 y no 
    // será posible llamar a ninguna función que tenga el modificador
    // `onlyOwner`
    function renounceOwnership() public onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    // Permite al owner acutal del contrato transferir este poder a otra
    // dirección
    function transferOwnership(address newOwner) public onlyOwner {
        require(newOwner != address(0), "Address must be != 0");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}