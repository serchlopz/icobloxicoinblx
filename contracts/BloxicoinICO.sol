pragma solidity >=0.4.21 <0.6.0;

import "./Controlled.sol";
import "./Ownable.sol";
import "./BloxicoinToken.sol";

contract BloxicoinICO is Ownable, Controller {
    uint256 constant public limit = 100 ether;
    uint256 constant public exchange = 1;
    uint256 public totalCollected;

    BloxicoinToken public bloxicoinToken;
    address public destEthers;

    
    constructor() public {
        totalCollected = 0;
    }


    modifier initialized() {
        require(address(bloxicoinToken) != address(0), "Not initialized");
        _;
    }


    function initialize(address _token, address _destEth) public {
        require(address(bloxicoinToken) == address(0), "Contract already initialized");
        bloxicoinToken = BloxicoinToken(_token);
        require(bloxicoinToken.totalSupply() == 0, "Contract already initialized");
        require(bloxicoinToken.controller() == address(this), "Contract already initialized");
        destEthers = _destEth;
    }

    
    function proxyPayment(address _th) public payable returns (bool success) {
        return doBuy(_th, msg.value);
    }


    function doBuy(address _sender, uint _amount) public returns (bool success) {
        uint256 tokensGenerated = _amount * exchange;

        require (totalCollected + _amount <= limit, "Limit reached");

        assert(bloxicoinToken.generateTokens(_sender, tokensGenerated));
        destEthers.transfer(_amount);

        totalCollected = totalCollected + _amount;

        return true;
    }
}