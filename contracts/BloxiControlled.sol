pragma solidity >=0.4.21 <0.6.0;
contract BloxiController {
    function proxyPayment(address _th) public payable returns (bool);
}

contract BloxiControlled {
    address public controller;

    constructor() public {
        controller = msg.sender;
    }

    modifier onlyController() {
        require (msg.sender == controller, "Caller is != controller");
        _;
    }
    
    function changeController(address _newController) public onlyController {
        controller = _newController;
    }
}