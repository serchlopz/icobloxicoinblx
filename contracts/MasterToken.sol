pragma solidity >=0.4.21 <0.6.0;

import "./Controlled.sol";

contract MasterToken is Controlled {
    
    constructor() public {
        controller = msg.sender;
    }

    mapping(address => uint256) balances;
    uint256 public totalSupply;

    function balanceOf(address _owner) public view returns (uint256 balance) {
        if (balances[_owner] == 0) {
            return 0;
        } else {
            return balances[_owner];
        }
    }

    function transfer(address _to, uint256 _value) public returns (bool success) {
        return doTransfer(msg.sender, _to, _value);
    }

    function transferFrom(address _from, address _to, uint256 _value) public onlyController returns (bool success) {
        return doTransfer(_from, _to, _value);
    }

    function doTransfer(address _from, address _to, uint256 _value) internal returns (bool success) {
        // Caso en el que se quieran transferir 0 tokens
        if (_value == 0) {
            return true;
        }

        // Comprobamos que la dirección a la que se quieran mandar sea correcta y que
        // no sea a este mismo contrato
        require (_to != 0 && _to != address(this), "Invalid _to address on doTransfer");

        // Comprobamos que el balance de la persona que manda es mayor a lo que se quiere
        // mandar, en caso contrario se devuelve la ejecución como fallida.
        // Si todo va correctamente sustraemos de la cuenta del remitente los tokens.
        uint256 previousBalanceFrom = balanceOf(_from);
        if (previousBalanceFrom < _value ) {
            return false;
        }
        balances[_from] = balances[_from] - _value;

        // Hacemos lo mismo para el destinatario. La línea importante aquí es el require
        // ya que estamos comprobando que no exista un overflow, es decir, que cuando
        // le sumemos al balance del destinatario el valor de los tokens enviados
        // el valor sea mayor de lo que puede almacenar el uint256
        uint256 previousBalanceTo = balanceOf(_to);
        require(previousBalanceTo + _value >= previousBalanceTo, "Overflow in previousBalanceTo + _value");
        balances[_to] = balances[_to] + _value;

        // Emitimos el evento 
        emit Transfer(_from, _to, _value);

        return true;
    }

    function isContract(address _addr) internal view  returns(bool) {
        uint size;
        if(_addr == 0) return false;

        assembly {
            size := extcodesize(_addr)
        }
        return (size > 0);
    }

    function () public payable {
        // Comprobamos que el controlador sea un contrato
        require(isContract(controller), "Controller is not a contract");

        // Creamos una instancia del contrato controlador con la dirección de este
        Controller c = Controller(controller);

        // Esta es probablemente la línea más complciada de entender, principalmente
        // lo que estamos haciendo es llamar al método proxyPayment del controlador
        // pero en vez de pasarle por argumentos el msg.value (ethers) y el msg.sender
        // lo hacemos mediante el método value, que lo que hace es mandar los ethers
        // como si se estuviese haciendo un pago, es decir, de la misma forma que le
        // han llegado a este contrato.
        require(c.proxyPayment.value(msg.value)(msg.sender), "Controller returned false");
    }

    function generateTokens(address _owner, uint _amount) public onlyController returns (bool) {
        uint curTotalSupply = totalSupply;
        
        // Comprobar que no existan overflows en las operaciones
        // con totalSupply
        require(totalSupply + _amount >= curTotalSupply, "totalSupply overflow error in generate tokens");
        uint previousBalanceTo = balanceOf(_owner);
        
        // Comprobar que no existan overflows en las operaciones
        // con previousBalanceTo
        require(previousBalanceTo + _amount >= previousBalanceTo, "previousBalanceTo overflow error in generate tokens");
        totalSupply = curTotalSupply + _amount;
        
        // Aumentar el balance de la dirección donde se van a generar
        // los tokens
        balances[_owner] = previousBalanceTo + _amount;

        emit Transfer(0, _owner, _amount);
        
        return true;
    }

    event Transfer(address _from, address _to, uint256 _value);
}