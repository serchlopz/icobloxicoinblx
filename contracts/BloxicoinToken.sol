pragma solidity >=0.4.21 <0.6.0;
import "./BloxiControlled.sol";

contract BloxicoinToken is BloxiControlled {
    struct BlxToken{
        string nombre;
        string symbol;
        uint8 decimals;
        uint256 totalSupply;
    }
    BlxToken public datos;

    mapping (address => uint256) internal balances;
    mapping (address => mapping (address => uint256)) internal allowed;
    uint256 public totalSupply;

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);

    constructor (string memory nombre, string memory symbol, uint8 decimals, uint256 totalSupply)  public {
        datos = BlxToken(nombre, symbol, decimals, totalSupply);
        balances[msg.sender] = totalSupply;
    }

    function getToken() public view returns(string memory, string memory, uint8, uint256){
        return(datos.nombre, datos.symbol, datos.decimals, datos.totalSupply);
    }

    function name() public view returns (string memory)
    {
        return datos.nombre;
    }

    function symbol()public view returns (string memory) {
        return datos.symbol;
    }

    function decimals()public view returns (uint8) {
        return datos.decimals;
    }

    function totalSupplyDatos() public view returns (uint256) {
        return datos.totalSupply;
    }
    function isContract(address _addr) internal view  returns(bool) {
        uint size;
        if(_addr == 0) return false;

        assembly {
            size := extcodesize(_addr)
        }
        return (size > 0);
    }
    function mul(uint256 a, uint256 b) internal pure returns (uint256)
    {
        if (a == 0) return 0;
        uint256 c = a * b;
        assert(c / a == b);
        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256)
    {
        uint256 c = a / b;
        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }

    function transfer(address _to, uint256 _value) public returns (bool) {
        require(_to != address(0));
        require(_value <= balances[msg.sender]);
        balances[msg.sender] = sub(balances[msg.sender], _value);
        balances[_to] = add(balances[_to], _value);
        emit Transfer(msg.sender, _to, _value);
        return true;
    }

    function balanceOf(address _owner) public view returns (uint) {
        return balances[_owner];
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool) {
        require(_to != address(0));
        require(_value <= balances[_from]);
        require(_value <= allowed[_from][msg.sender]);

        balances[_from] = sub(balances[_from], _value);
        balances[_to] = add(balances[_to], _value);
        allowed[_from][msg.sender] = sub(allowed[_from][msg.sender], _value);
        emit Transfer(_from, _to, _value);
        return true;
    }

    function approve(address _spender, uint256 _value) public returns (bool) {
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function approveFromOwner(address _owner,address _spender, uint256 _value) public returns (bool) {
        allowed[_owner][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) public view returns (uint256) {
        return allowed[_owner][_spender];
    }

    function increaseApproval(address _spender, uint _addedValue) public returns (bool) {
        allowed[msg.sender][_spender] = add(allowed[msg.sender][_spender], _addedValue);
        emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
        return true;
    }
    function generateTokens(address _owner, uint _amount) public onlyController returns (bool) {
        uint curTotalSupply = totalSupply;
        
        // Comprobar que no existan overflows en las operaciones
        // con totalSupply
        require(totalSupply + _amount >= curTotalSupply, "totalSupply overflow error in generate tokens");
        uint previousBalanceTo = balanceOf(_owner);
        
        // Comprobar que no existan overflows en las operaciones
        // con previousBalanceTo
        require(previousBalanceTo + _amount >= previousBalanceTo, "previousBalanceTo overflow error in generate tokens");
        totalSupply = curTotalSupply + _amount;
        
        // Aumentar el balance de la dirección donde se van a generar
        // los tokens
        balances[_owner] = previousBalanceTo + _amount;

        emit Transfer(0, _owner, _amount);
        
        return true;
    }
}
